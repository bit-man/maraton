/***************************************************************************/
/*                                                                         */
/*                    Algoritmos y Estructuras de Datos II                 */
/*                              TP4  -  1C 2007                            */
/*                                                                         */
/*                 Clase Maraton                 Version 0.1               */
/*                                                                         */
/***************************************************************************/



#ifndef __MARATON_H__
#define __MARATON_H__

// Esto vuelca todo el namespace de la biblioteca standard al namespace global.
// Si se elimina habr� que poner 'std::' antes de cada nombre de la stdlib.
using namespace std;

#include "tipos.h"
#include <string>
#include <iostream>


class Maraton {
	public:
	// generadores
	Maraton(const ConjAnimales&, Distancia);
    // Constructor por copia
	Maraton(const Maraton&);
    void animar(const ConjNombres&);
    ~Maraton();

	// observadores
    const ConjAnimales& participantes() const;
    Distancia distanciaTotal() const;
    Distancia cuantoCorrio(const Nombre&) const;
    const ListaPosiciones& ordenDeLlegada() const;
    const ConjAnimales& animalesEnPos(Posicion);

	// operador de asignaci�n
	Maraton& operator=(const Maraton&);
    // Igualdad y desigualdad de maratones.
	bool operator==(const Maraton&) const;
	bool operator!=(const Maraton&) const;
		
    // Muestra una Maraton en pantalla. Debe listar todos los participantes
    // quienes terminaron cuales son los nombres de animals en cada posicion de la 
    // carrera.
	friend ostream& operator<<(ostream&, const Maraton&);

 	private:
	  // ...
};

#endif

