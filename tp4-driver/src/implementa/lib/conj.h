#ifndef _CONJ_H
#define _CONJ_H

#include <set>
#include <cassert>

using namespace std;

template <class T>
class Conj:public set<T> {
// notar y recordar que todas las operaciones tienen
// que ser referencialmente transparentes porque las
// vamos a usar con la especificaci'on compilada.
public:
	Conj();
/*	Conj<T> ag(const T &) const;
	bool esVacio() const;
	bool pertenece(const T&) const;
	T dameUno() const;
	Conj<T> sinUno() const;
	// no le puedo poner "union" porque es una palabra reservada =(
	Conj<T> unir(const Conj<T>&) const;
	Conj<T> operator-(const Conj<T>&) const;
	
  */
	template<typename R> friend std::istream& operator>>(std::istream& is, Conj<R>& conj);
};

template<typename R> std::istream& operator>>(std::istream& is, Conj<R>& conj)
{
  char nextChar;
  is >> nextChar;
  assert(nextChar == '{');

  while(true) {
    is >> nextChar;
    if(nextChar == '}')
      break;
    is.putback(nextChar);
    R in;
    is >> in;
    conj.insert(in); 
  }
  return is;
}

template <class T>
Conj<T>::Conj() {
}
/*
template <class T>
Conj<T> Conj<T>::ag(const T &x) const {
	Conj<T> nuevo;

	//nuevo._conj = _conj;
	nuevo.insert(x);
	return nuevo;
}

template <class T>
bool Conj<T>::esVacio() const {
	return empty();
}

template <class T>
T Conj<T>::dameUno() const {
	assert(!esVacio());
	return begin();
}

template <class T>
Conj<T> Conj<T>::sinUno() const {
	assert(!esVacio());
	//Conj<T> nuevo;
	//nuevo.j = _conj;
	nuevo.erase(nuevo.begin());
	return nuevo;
}

template <class T>
bool Conj<T>::pertenece(const T &x) const {
	return count(x) != 0;
}

template <class T>
Conj<T> Conj<T>::unir(const Conj<T> &otro) const {
	Conj<T> nuevo;
	//nuevo._conj = _conj;
	for (typename set<T>::const_iterator i = otro.begin();
		i != otro.end(); ++i) {
		nuevo.insert(*i);
	}
	return nuevo;
}

template <class T>
Conj<T> Conj<T>::operator-(const Conj<T> &otro) const {
	Conj<T> nuevo;
	for (typename set<T>::const_iterator i = begin();
		i != end(); ++i) {
		if (otro.count(i) == 0)
			nuevo.insert(*i);
	}
	return nuevo;
}
*/
#endif /*_CONJ_H*/
