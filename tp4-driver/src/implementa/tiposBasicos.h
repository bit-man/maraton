#ifndef TIPOSBASICOS_H
#define TIPOSBASICOS_H
#include<assert.h>

#include<string>
#include<iostream>

using namespace std;

typedef string Nombre;
typedef unsigned long int nat;
typedef unsigned long int Posicion;
typedef unsigned long int Distancia;

ostream& operator<<(ostream& os, const ConjNombres& output);
ostream& operator<<(ostream& os, const ConjAnimales& output);
ostream& operator<<(ostream& os, const ListaPosicion& output);

istream& operator>>(istream& is, ConjNombres& input);
istream& operator>>(istream& is, ConjAnimales& input);
istream& operator>>(istream& is, ListaPosicion& input);


#endif
