// v1.3.0

#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include "cmd.h"
#include "util.h"
#include "../implementa/Maraton.h"
#include "id.h"

using std::string;

enum Type { 			
	// -- TAD Maraton --------------
    largar, animar, participantes, distanciaTotal,
    cuantoCorrio, ordenDeLlegada,
	animalesEnPos,
    // -- comandos auxiliares -------------
	crearConjAnimales, crearConjNombres, crearListaPosicion
};

class Decoder 
{
private:    
	std::map<string, Type> codes;
public:    	
	Decoder();
    Type operator()(string cmdTypeStr) const throw (BadTypeException) 
    {
    	if (codes.find(toupperstr(cmdTypeStr)) == codes.end())
			throw BadTypeException("comando desconocido: " + cmdTypeStr);
        return codes.find(cmdTypeStr)->second;
	}
};

void cmd::exec(string cmdln, ObjectTable& symbolTable, Result& result) 
	throw (UsageException, BadTypeException, InvalidTestException, 
		ErrorTestException, std::ios_base::failure)
// TODO rastrear d�nde se usa InvalidTestException, si es que se usa,
// y corregir si no. deber�a servir para cuando est� malformado un test,
// como xej cuando hay error de tipos o mala cantidad de argumentos.
{
	if (result.getType() != Result::ok and result.getType() != Result::unknown)
		throw UsageException("Se quiere ejecutar un comando luego de una situacion irrecuperable");

	istringstream ist(cmdln);	
	ist.exceptions(ios_base::badbit | ios_base::failbit);
	boolalpha(ist);						// usar true y false en vez de 1 y 0
	string cmd;
	ist >> cmd;
	Decoder decode;

	// declaraci�n de variables que figuran en los comandos
	Id<Maraton> idMaraton;
    Id<ConjAnimales> idConjAnimales, idCAEsperado;
    Id<ConjNombres> idConjNombres;
    Id<ListaPosicion> idListaPosicion, idLPEsperada;

	Distancia distanciaEsperada, distanciaObtenida;
	Posicion posicion;
	Distancia distancia;
	// bool boolEsperado, boolObtenido;
    ListaPosicion lpEsperada;
    ListaPosicion lpObtenida;
    ListaPosicion *listaPosicion = NULL;
    Nombre nombre, nombreEsperado, nombreObtenido;
    ConjAnimales caEsperado, caObtenido;
    ConjAnimales *conjAnimal = NULL;
    ConjNombres *conjNombre = NULL;

	switch(decode(cmd)) {

	/* comentarios: 
	 * 	
	 * - cuando un comando recibe o devuelve tipos primitivos, lo hace 
	 * 	 siempre por copia; mientras que cuando manipula tipos complejos lo 
	 *   hace siempre por referencia.
	 * - las funciones que en los TADs reciben una instancia de un tipo 
	 * 	 complejo y devuelven otra, en la implementaci�n reciben el 
	 * 	 par�metro por referencia y lo modifican.
	 * - al modificar un par�metro complejo recibido por referencia, 
	 *   probablemente se aprovechen efectos colaterales del aliasing.
	 * - los nombres son casesensitive.
	 * - no se verifica que se cumplan las precondiciones.
	 * - todo lo que siga a continuaci�n de los comandos necesarios, se 
	 * 	 ignora.
	 * - los generadores no recursivos se implementaron como constructores.
	 * - los generadores recursivos NO se implementaron como constructores,
	 *   sino como funciones miembro.
	 * - como siempre, si surgen, preguntar.
	 */
	 
	/* TODO:
	 * - verificar que se detectan efectivamente los par�metros de menos.
	 * - verificar qu� pasa con par�metros de m�s.
	 * - podr�a tratar de integrar el criterio a las anotaciones.
	 * - podr�a agregarse una anotaci�n para constructores de tipos 
	 * 	 auxiliares, considerando en especial los casos Tupla y Colecci�n.
	 * - podr�a agregarse una anotaci�n sobre los constructores por defecto.
	 */
	
	// -- Maraton -----------------------
	// observadores
	case participantes:			// Maraton -> ConjAnimales
		// esParticipante idMaraton ConjAnimales
		ist >> idMaraton >> caEsperado;
		caObtenido = symbolTable[idMaraton].participantes(); 
		assertEquals(caEsperado, caObtenido);  
		break;
	case distanciaTotal:			// Maraton -> Nat
		// distanciaTotal idMaraton nat
		ist >> idMaraton >> distanciaEsperada;
		distanciaObtenida = symbolTable[idMaraton].distanciaTotal(); 
		assertEquals(distanciaEsperada, distanciaObtenida);
		break;
	case cuantoCorrio: // Maraton x Nombre -> nat
		// cuantoCorrio idMaraton Nombre Distancia
		ist >> idMaraton >> nombre >> distanciaEsperada;
		distanciaObtenida = symbolTable[idMaraton].cuantoCorrio(nombre);
		assertEquals(distanciaEsperada, distanciaObtenida);
		break;
	case ordenDeLlegada: // Maraton -> Secu(Conj(Nombre))
		// ordenDeLlegada Maraton ListaPosicion
		ist >> idMaraton >> lpEsperada;
		lpObtenida = symbolTable[idMaraton].ordenDeLlegada();
		assertEquals(lpEsperada, lpObtenida);
		break;
	case animalesEnPos: // Maraton x nat -> ConjAnimales
		// animalesEnPos Maraton nat ConjAnimales
		ist >> idMaraton >> posicion >> caEsperado;
		caObtenido = symbolTable[idMaraton].animalesEnPos(posicion);
		assertEquals(caEsperado, caObtenido);
		break;

	//generadores
	case largar: // ConjAnimaleses x Distancia -> Maraton
		ist >> idConjAnimales >> distancia >> idMaraton;
		symbolTable.define(idMaraton,
				new Maraton(symbolTable[idConjAnimales], distancia));
		break;
	case animar: // Maraton x ConjNombres -> Maraton
		ist >> idMaraton >> idConjNombres;
		symbolTable[idMaraton].animar(symbolTable[idConjNombres]);
		break;
	
	// -- comandos auxiliares -------------
	case crearConjAnimales:
		conjAnimal = new ConjAnimales();
		ist >> *conjAnimal >> idConjAnimales;
		symbolTable.define(idConjAnimales, conjAnimal);
		break;
	case crearConjNombres:
		conjNombre = new ConjNombres();
		ist >> *conjNombre >> idConjNombres;
		symbolTable.define(idConjNombres, conjNombre);
		break;
	case crearListaPosicion:
		listaPosicion = new ListaPosicion();
		ist >> *listaPosicion >> idListaPosicion;
		symbolTable.define(idListaPosicion, listaPosicion);
		break;	
	default:
		throw UsageException("No est� contemplado el comando: " + cmd);
	}
}

Decoder::Decoder() 
{
	// �CUIDADO! todas las cadenas deben estar en may�sculas.
	
	// -- TAD Maraton ---------------
	codes["PARTICIPANTES"]      =  participantes;
	codes["DISTANCIATOTAL"]     =  distanciaTotal;
	codes["CUANTOCORRIO"]       =  cuantoCorrio;
	codes["ORDENDELLEGADA"]     =  ordenDeLlegada;

	// GENERADORES
	codes["LARGAR"]             =  largar;
	codes["ANIMAR"]             =  animar;
	
	// OTRAS OPERACIONES
	codes["ANIMALESENPOS"]         =  animalesEnPos;
	
	// -- comandos auxiliares -------
	codes["CREARCONJANIMALES"]  =  crearConjAnimales;
	codes["CREALRISTAPOSICION"] =  crearListaPosicion;
	codes["CREARCONJNOMBRES"]   =  crearConjNombres;
}

// vim:ts=4:sw=4:
