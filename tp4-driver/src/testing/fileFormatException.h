// fileFormatException.h : 
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -23may2004- creado

#ifndef FILE_FORMAT_EXCEPTION_H
#define FILE_FORMAT_EXCEPTION_H

#include "exception.h"

class FileFormatException : public Exception
{
private:
	const string fname;		// file name
public:
	FileFormatException(string fileName, string message = ""): 
		Exception(message), fname(fileName) { }
    string fileName() const { return fname; }
};

#endif
