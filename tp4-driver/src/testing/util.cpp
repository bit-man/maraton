// v1.1.0

#include "util.h"
#include <sstream>
#include <iomanip>

string& tolowerstr(string& s) {
	for (string::iterator si = s.begin(); si != s.end(); si++)
		*si = tolower(*si);
	return s;									
}

string& toupperstr(string& s) {
	for (string::iterator si = s.begin(); si != s.end(); si++)
		*si = toupper(*si);
	return s;									
}

string& trim(string& s) {
	const string white = " \t\v";	// SPACE, TAB, VERTICAL TAB
	string::size_type 
		ini = s.find_first_not_of(white),
		end = s.find_last_not_of(white) - ini + 1;
	return (s = (ini <= end)? string(s, ini, end): "");
	// todo: cuidar npos; ver Stroustrup 20.3.11
}

// todo: revisar si estas funciones se puedan implementar usando for_each.
