// testcasedrv.h : maneja un s�lo caso de test.
// v1.0.2 -24oct2005- cambio de aridad en cmd::exec()
// v1.0.1 -10nov2004- versi�n publicable
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -23may2004- creado

#ifndef TEST_CASE_DRV_H
#define TEST_CASE_DRV_H

#include <string>
#include "result.h"
#include "exception.h"

namespace TestCaseDrv
{
	using std::string;
	Result exec(const string& fileName) throw (Exception);
}

#endif
