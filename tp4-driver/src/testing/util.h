// util.h : funciones �tiles
// v1.1.0 -24oct2005- se agreg� assertEquals() y se emprolij� el c�digo.
// v1.0.1 -10nov2004- se agregaron comentarios al c�digo
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.0 -23may2004- creado

#ifndef UTIL_H
#define UTIL_H
#include <string>
#include <sstream>
#include <iomanip> 
#include "errorTestException.h"

using std::string;

// modifica el parametro recibido
// devuelve ref para poder encadenar a otras fn; xej: cout << tolowerstr(s);
string& tolowerstr(string& s);

// modifica el parametro recibido
// devuelve ref para poder encadenar a otras fn; xej: cout << toupperstr(s);
string& toupperstr(string& s);

// modifica el parametro recibido
// devuelve ref para poder encadenar a otras fn; xej: cout << trim(s);
string& trim(string& s); 

// convierte un tipo T, que sabe manejarse con streams, en un string
template<class T> string toString(T x)
{
	std::ostringstream oss;
	// si la linea de abajo no compila, probablemente tengas un compilador viejo. 
	// gcc 3.2.3 funciona seguro y el compilador del ms devstudio 2003 tambi�n.
	boolalpha(oss);	//oss << boolalpha;
	oss << x;
	return oss.str();
}

template < typename T >
void assertEquals(const T& expected, const T& actual) 
throw (ErrorTestException)
{
	if (expected != actual)
		throw ErrorTestException("esperado: " + toString<T>(expected) + '\t' +
				"obtenido: " + toString<T>(actual));
}

#endif
