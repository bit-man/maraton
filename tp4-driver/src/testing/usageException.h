// usageException.h : 
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -20may2004- creado

#ifndef USAGE_EXCEPTION_H
#define USAGE_EXCEPTION_H

#include "exception.h"

class UsageException : public Exception
{
public:
	UsageException(string message = ""): Exception(message) { }
};

#endif
