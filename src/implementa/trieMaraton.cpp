#include "trieMaraton.h"

ostream& operator<< (ostream& salida, const trieMaraton &elem){
    salida << elem.elementos;
    return salida;        
}

istream& operator>> (istream& entrada, trieMaraton &elem){
    entrada >> elem.elementos;
    return entrada;        
}

bool trieMaraton::operator==(const trieMaraton &elOtro) const{
   return elementos == elOtro.elementos;       
}

trieMaraton& trieMaraton::operator=(const trieMaraton &elOtro){
   elementos = elOtro.elementos;
   return *this;
}

void trieMaraton::CrearTrieM(conjlog<animal> animales){
    conjlog<animal>::iterador_const it(animales);
	while (it.valido()) {
        animalInfo temp;
        temp.cuantoCorre = it.actual().cuantoCorre;
        temp.cuantoCorrio = 0;
        temp.posicion = 1;
		elementos.Definir(it.actual().nombre,temp);
		it.siguiente();
	}
}
	
bool trieMaraton::Definido(string clave){
     return elementos.Definido(clave);
}

bool trieMaraton::Obtener(string clave, animalInfo &animal) const{
     return elementos.Obtener(clave, animal);
}

void trieMaraton::Claves(conjlog<string> &destino) const{
     elementos.Claves(destino);
}

void trieMaraton::Animales(conjlog<animal> &destino) const{
    conjlog<string> temp;
    Claves(temp);
    conjlog<string>::iterador_const it(temp);
	while (it.valido()) {
		string nombre = it.actual();
		animalInfo temporario;
        Obtener(nombre, temporario);
        int cuantoCorre = temporario.cuantoCorre;
        animal animalActual;
        animalActual.nombre = nombre;
        animalActual.cuantoCorre = cuantoCorre;
        destino.agregar(animalActual);
		
        it.siguiente();
	}
}

void trieMaraton::Modificar(string nombre, int cuantoCorrio, int posicion){
    animalInfo temporario;
    Obtener(nombre, temporario);
    temporario.cuantoCorrio = cuantoCorrio;
    temporario.posicion = posicion;
    elementos.Modificar(nombre, temporario);
}
