#include "tipos_animal.h"

/*
 * TODO: "TODOS sus tipos deben ofrecer
 * los operadores << y >> para entrada salida, as� como tambi�n los
 * operadores de asignaci�n, construcci�n por copia, y operadores == y !="
 */

/*
 *  Clase animal
 *
 *  Constructor por copia: el default sirve ya que tanto string como int no son punteros
 *  operator= : el default ya sirve
 */


 
bool animal::operator==(const animal &otro) const{
     return (nombre == otro.nombre && cuantoCorre == otro.cuantoCorre);
}

bool animal::operator!=(const animal &otro) const{
     return (nombre != otro.nombre || cuantoCorre != otro.cuantoCorre);
}

bool animal::operator<(const animal & otro) const{
    return nombre < otro.nombre;
}

ostream& operator<<(ostream& salida, const animal& elem) {
     salida << "< " << elem.nombre << " " << elem.cuantoCorre << " >";
     return salida;
}

istream& operator>>(istream& entrada, animal& elem) {
	char delimiter;
	string nombre;
	int cuantoCorre;
	
	entrada >> delimiter; // espero que sea un '<'
	
	entrada >> nombre;  
	elem.nombre = nombre;
	
	entrada >> cuantoCorre;
	elem.cuantoCorre = cuantoCorre;
	
	entrada >> delimiter; // espero que sea un '>'
	
	return entrada;
};


/*
 *  Clase animalInfo
 * 
 *  Constructor por copia: el default sirve ya que tanto string como int no son punteros
 *  operator= : el default ya sirve
 */

bool animalInfo::operator==(const animalInfo &elOtro) const{
    return ( (cuantoCorre == elOtro.cuantoCorre) && 
    		 (cuantoCorrio == elOtro.cuantoCorrio) &&
             (posicion == elOtro.posicion)
           );
}

bool animalInfo::operator!=(const animalInfo &elOtro) const{
    return ( (cuantoCorre != elOtro.cuantoCorre) || 
    		 (cuantoCorrio != elOtro.cuantoCorrio) ||
             (posicion != elOtro.posicion)
           );
}


bool animalInfo::operator<(const animalInfo & otro) const{
    return ( (cuantoCorre < otro.cuantoCorre) &&
    		 (cuantoCorrio < otro.cuantoCorrio) &&
             (posicion < otro.posicion)
           );
}

ostream& operator<<(ostream& salida, const animalInfo& elem) {
     salida << "< " << elem.cuantoCorre << " " << elem.cuantoCorrio << " " << elem.posicion << " >";
     return salida;
}

istream& operator>>(istream& entrada, animalInfo& elem) {
	char delimiter;
	int numero;
	
	entrada >> delimiter; // espero que sea un '<'
	
	entrada >> numero;  
	elem.cuantoCorre = numero;
	
	entrada >> numero;
	elem.cuantoCorrio = numero;

	entrada >> numero;
	elem.posicion = numero;
	
	entrada >> delimiter; // espero que sea un '>'
	
	return entrada;
};
