#ifndef TIPOS_H
#define TIPOS_H

/* 
 * NOTA: es importante cuidar MUCHO el orden
 * en que se definen las inclusiones.
 */ 

// ---- TIPOS B�SICOS ----
// typedefs de tipos primitivos sueltos... 

// ToDo: Por ahora lo pongo para que compile, pero despues lo cambio
//       Ver cuales se usan y cuales no
using namespace std;

#include "tiposBasicos.h"
#include "trieMaraton.h"

// #includes de (tuplas de) tipos primitivos... 

// ---- COLECCIONES DE TIPOS B�SICOS ----
// algunas colecciones son tipos b�sicos,
// seg�n lo sea el elemento almacenado.
// #includes de contenedores para tipos b�sicos...
// #include "lib/conjlog.h"
// #include "lib/dicclog.h"
// #include "lib/secu.h"

// typedefs de colecciones de tipos b�sicos...
//
// Ejemplo:
//
// typedef conjlog<Animal> ConjAnimales;
// typedef ...
//
// (el driver usa ConjAnimales, ConjNombres,
// ListaPosiciones, etc. hay que definirlos)

#endif // TIPOS_H
