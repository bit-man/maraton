#ifndef _MARATON_H
#define _MARATON_H

#include <iostream>
#include <string>

#include "tipos.h"

/*
 * TODO: "TODOS sus tipos deben ofrecer
 * los operadores << y >> para entrada salida, as� como tambi�n los
 * operadores de asignaci�n, construcci�n por copia, y operadores == y !="
 */
 
class Maraton{
    public:
	  // generadores
	  Maraton(const ConjAnimales&, Distancia);
      // Constructor por copia
	  Maraton(const Maraton&);
      void animar(const ConjNombres&);
      ~Maraton();

	  // observadores
      const ConjAnimales participantes() const;
      Distancia distanciaTotal() const;
      Distancia cuantoCorrio(const Nombre&) const;
      const ListaPosicion& ordenDeLlegada() const;
      const ConjAnimales animalesEnPos(Posicion);

	  // operador de asignaci�n
	  Maraton& operator=(const Maraton&);
      // Igualdad y desigualdad de maratones.
	  bool operator==(const Maraton&) const;
	  bool operator!=(const Maraton&) const;
		
      // Muestra una Maraton en pantalla. Debe listar todos los participantes
      // quienes terminaron cuales son los nombres de animals en cada posicion de la 
      // carrera.
      // TODO: cambiar para que se ajuste al testing !!!
	  friend ostream& operator<<(ostream&, const Maraton&);
      friend istream& operator>>(istream&, Maraton&);
      
 	private:
      void copiaMaraton(const Maraton&);
	  trieMaraton animales;
	  nat indiceCabecera;
	  ListaPosicion ordenLlegada;
	  ArregloPosiciones posiciones;
	  nat tamPosiciones;
	  Distancia distancia;	  
      
      // Usadas por animar()
    void modificarArrayGano(nat);
    void modificarArrayNuevaCabecera( Nombre, Distancia, nat);
    void modificarArray( Nombre, Distancia, nat);
    Distancia distDelSiguiente(int, nat);
    void ponerDondeVaya( Nombre, Distancia,nat);
    void correrArrayParaAdelante(nat);
    void correrArrayParaAtras(nat,int);
    nat posDeN(Nombre);
    Distancia distCabecera(const conjlog<Nombre>);
};

#endif
