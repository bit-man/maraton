#ifndef _TIPOS_ANIMAL_H
#define _TIPOS_ANIMAL_H

#include <cstdlib>
#include <iostream>
using namespace std;

// ToDo: para que sea m�s robusto generar un constructor. Ademas as� no tiene
//		 otras funcionalidades no tiene diferenciacion con un struct

class animal {
public:
    string nombre;
    int cuantoCorre;
    bool operator==(const animal &otro) const;
    bool operator!=(const animal &otro) const;
    bool operator<(const animal & otro) const;
    friend ostream& operator<<(ostream& salida, const animal& elem);
	friend istream& operator>>(istream& entrada, animal& elem);
};

class animalInfo {
public:
    int cuantoCorre;
    int cuantoCorrio;
    int posicion;
    bool operator==(const animalInfo &otro) const;
    bool operator!=(const animalInfo &otro) const;
    bool operator<(const animalInfo & otro) const;
    friend ostream& operator<<(ostream& salida, const animalInfo& elem);
    friend istream& operator>>(istream& entrada, animalInfo& elem);
};

#endif
