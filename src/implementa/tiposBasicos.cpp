
#include <iostream>
#include <string>

#include "tipos.h"

using namespace std;


istream& operator>>(istream& is, ListaPosicion& input) {
    /*
     * Entrada esperada :  
     *          '[ ConjNombre1 ConjNombre2 ... ConjNombreN ]'
     */

	waitForChar( '[', is );
	skipChar( ' ', is ); 

    while( is.peek() != ']' ) {
        ConjNombres cn;
        is >> cn;
        input.insertarAlFinal( cn );

		skipChar( ' ', is );
    }

    is.get(); // Espero un ']', y si no es la stdc++ me est� mintiendo :-P

    return is;
};


istream& operator>>(istream& is, ConjNombres& input) {
    /*
     * Entrada esperada :  
     *          '{ nombre1 nombre2  ... nombreN }'
     */
    string nombre;

	waitForChar( '{', is );
	skipChar( ' ', is );

    while( is.peek() != '}' ) {
        is >> nombre;
        input.agregar( nombre );
        skipChar( ' ', is );
    }

    is.get(); // Espero un '}', y si no es la stdc++ me est� mintiendo :-P

    return is;
};

istream& operator>>(istream& is, ConjAnimales& input) {
   //Ej de lo que recibe: " { < vaca 5 > } idConjAnimales"

	waitForChar( '{', is );
	skipChar( ' ', is );

   // Pone a todos los animales que encuentra entre los corchetes 
   // en el conj 'input'
   while( is.peek() != '}' ) {
     animal actual;
     is >> actual;
     input.agregar( actual );
     skipChar( ' ', is );  
   }

   is.get();    //Vendria a ser el char: '}'
   return is;
};

// Avanza sobre el istream y para cuando encuentra el char especificado
void waitForChar( char cWait, istream& is ) {
	char ch;

	is.get(ch);
	while(  ch != cWait ) { is.get(ch); };
};

// Avanza sobre el istream y para cuando "no" encuentre el char especificado
void skipChar( char cSkip, istream& is ) {
	char ch;
	
	while(  is.peek() == cSkip ) { 
		is.get(ch); 
	};
};

// TODO: sacar lo siguiente que est� comentado
// Ya implementada en secu.h
/*
ostream& operator<<(ostream& os, const ListaPosicion& output) {
    ListaPosicion::iterador_const it(output);
    os << "[ ";
	while (it.valido()) {
        os<<it.actual()<<" ";
        it.siguiente();
	}
    os << "]" ;    
	return os;
};
*/

// Ya implementada en conjlog.h
/*
ostream& operator<<(ostream& os, const ConjNombres& output) {
    ConjNombres::iterador_const it(output);
    os << "{ ";
	while (it.valido()) {
        os << it.actual() << " ";
        it.siguiente();
	}
    os << "}";     
	return os;
};

ostream& operator<<(ostream& os, const ConjAnimales& output){
    ConjAnimales::iterador_const it(output);
    os << "{ ";
	while (it.valido()) {
        os << it.actual() << " ";
        it.siguiente();
	}
    os << "}";     
	return os;      
};
*/
