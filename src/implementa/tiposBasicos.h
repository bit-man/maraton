#ifndef TIPOSBASICOS_H
#define TIPOSBASICOS_H
#include<assert.h>

#include<string>
#include<iostream>

#include "tipos_animal.h"
#include "lib/conjlog.h"
#include "lib/dicclog.h"
#include "lib/secu.h"


using namespace std;


typedef string Nombre;
typedef unsigned long int nat;
typedef unsigned long int Posicion;
typedef unsigned long int Distancia;

typedef animal Animal;

typedef conjlog<Animal> ConjAnimales;
typedef conjlog<Nombre> ConjNombres;
typedef secu< ConjNombres > ListaPosicion;
typedef ConjNombres *ArregloPosiciones;

// Ya implementada en conjlog.h
// ostream& operator<<(ostream& os, const ConjNombres& output);
// ostream& operator<<(ostream& os, const ConjAnimales& output);

// Ya implementada en secu.h
//ostream& operator<<(ostream& os, const ListaPosicion& output);

istream& operator>>(istream& is, ConjNombres& input);
istream& operator>>(istream& is, ConjAnimales& input);
istream& operator>>(istream& is, ListaPosicion& input);

void waitForChar( char, istream& );
void skipChar( char, istream& );

#endif
