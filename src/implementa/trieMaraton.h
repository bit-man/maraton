#ifndef _TRIE_MARATON_H
#define _TRIE_MARATON_H

#include "trie.h"
#include "tipos_animal.h"

using namespace std;

class trieMaraton{
public: 
        void CrearTrieM(conjlog<animal>);
        bool Definido(string);
        bool Obtener(string, animalInfo&) const;
        void Claves(conjlog<string>&) const;
        void Animales(conjlog<animal>&) const;
        void Modificar(string, int cuantoCorrio, int posicion);
        friend ostream& operator<< (ostream& salida, const trieMaraton &elem);
        friend istream& operator>> (istream& entrada, trieMaraton &elem);
        bool operator==(const trieMaraton &elOtro) const;
        trieMaraton& operator=(const trieMaraton &elOtro);
private:
        trie<animalInfo> elementos;        
};



#endif
