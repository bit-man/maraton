#include "maraton.h"

using namespace std;

Maraton::Maraton(const ConjAnimales& ca, Distancia dist) {
      
      distancia = dist;
      animales.CrearTrieM(ca);
      indiceCabecera = 0;
      conjlog<Nombre> cn;
      animales.Claves(cn);
    //Est� en las precondiciones que no se cumpla este if, pero igual el testing  
    //crea una maraton con un conj vacio y sino explota
      if( ! ca.esVacio() )
      	tamPosiciones = cn.cardinal();
      else
      	tamPosiciones = 1;
      posiciones = new (conjlog<Nombre>[tamPosiciones]);
      posiciones[0] = cn;

}; 

// Constructor por copia
Maraton::Maraton(const Maraton &mACopiar) {
  copiaMaraton(mACopiar);
};

// Operador de asignaci�n
Maraton& Maraton::operator=(const Maraton &mACopiar){
  if ( this != &mACopiar ) {
     delete [] posiciones;
     copiaMaraton(mACopiar);
  }
  return *this;
};

void Maraton::copiaMaraton(const Maraton &mACopiar){
  distancia = mACopiar.distancia;
  indiceCabecera = mACopiar.indiceCabecera;
  tamPosiciones = mACopiar.tamPosiciones;
  posiciones = new conjlog<Nombre>[tamPosiciones];
  unsigned int i;
  for( i=0 ; i < mACopiar.tamPosiciones; i++)
      posiciones[i] = mACopiar.posiciones[i];
  animales = mACopiar.animales;
  ordenLlegada = mACopiar.ordenLlegada;
};

// Destructor
Maraton::~Maraton() {
	delete [] posiciones;
};

// Igualdad y desigualdad de maratones.
bool Maraton::operator==(const Maraton &mAComparar) const{
   bool tamPosicionesIguales = (tamPosiciones == mAComparar.tamPosiciones);
   bool elementosDePosicionesIguales = true;
   if (tamPosicionesIguales){
      unsigned int i;
      for( i=0; i<tamPosiciones && elementosDePosicionesIguales; i++)
          elementosDePosicionesIguales = (posiciones[i] == mAComparar.posiciones[i]);
   }
   bool animalesIguales = (animales == mAComparar.animales);
   bool distanciasIguales = (distancia == mAComparar.distancia);
   bool indicesCabecerasIguales = (indiceCabecera == mAComparar.indiceCabecera);
   bool ordenesDeLlegadaIguales = (ordenLlegada == mAComparar.ordenLlegada);
   
   return (tamPosicionesIguales && elementosDePosicionesIguales 
           && animalesIguales && distanciasIguales 
           && indicesCabecerasIguales && ordenesDeLlegadaIguales);
    
};

bool Maraton::operator!=(const Maraton &mAComparar) const{
  return !(*this==mAComparar);
};

// Observadores
const ConjAnimales Maraton::participantes() const {
	ConjAnimales ca;
	animales.Animales(ca);
	return ca;
};

Distancia Maraton::distanciaTotal() const {
	return distancia;
};

Distancia Maraton::cuantoCorrio(const Nombre& nombre) const {
    animalInfo animalEnCuestion;
    animales.Obtener(nombre, animalEnCuestion);
	return animalEnCuestion.cuantoCorrio;
};

const ListaPosicion&  Maraton::ordenDeLlegada() const {
	return ordenLlegada;
};

/*En el dise�o devolviamos como animales sus nombres, no un conj de animal,
pero el testing requiere que se devuelva un conjunto de tipo animal, eso cambia
el dise�o de esta funci�n bastante*/
const ConjAnimales Maraton::animalesEnPos(Posicion p) {
	nat posArray = (indiceCabecera + p-1) % tamPosiciones;
	ConjAnimales animalesResult;
	/*Itero por cada nombre del conjunto de nombres que estan en la posicion pedida
	  y por cada uno obtengo cuanto corre y luego lo agrego al conjunto de animales
      que se devuelve*/
    ConjNombres::iterador_const it(posiciones[posArray]);
	while (it.valido()) {
		Nombre nombre = it.actual();
		animalInfo temporario;
        animales.Obtener(nombre, temporario);
        int cuantoCorre = temporario.cuantoCorre;
        animal animalActual;
        animalActual.nombre = nombre;
        animalActual.cuantoCorre = cuantoCorre;
        animalesResult.agregar(animalActual);
        it.siguiente();
	}
	
    return animalesResult;
};

		
// Muestra una Maraton en pantalla. Debe listar todos los participantes
// quienes terminaron cuales son los nombres de animales en cada posicion de la 
// carrera.
ostream& operator<<(ostream &salida, const Maraton& m){
  /*
   * El formato de salida de la marat�n est� dado por las estructuras internas 
   * de la misma, ya que tanto esta como operator>> son usadas por el framework
   * de test para inspeccionar y crear, respectivamente, una marat�n :
   *
   *     distancia tamPosiciones indiceCabecera animales ordenLlegada posiciones
   *
   * por ejemplo :
   *
   * 452 2 0 <[tito] [< 74 0 0 >]><[pefp87df2pe] [< 3434 0 0 >]> [ ] { < tito > < pefp87df2pe > }
   */
  
  salida << m.distancia << " " << m.tamPosiciones << " " << m.indiceCabecera << " ";
  salida << m.animales  << " " << m.ordenLlegada  << " " << *(m.posiciones);
  return salida;
};


istream& operator>>(istream& entrada, Maraton& m) {
   /*
    * El formato esperado es el que se genera con operator>>, esper�ndose los
    * siguientes atributos privados :
    *
    *     distancia tamPosiciones indiceCabecera animales ordenLlegada posiciones
    *
    * por ejemplo :
    *
    * 452 2 0 <[tito] [< 74 0 0 >]><[pefp87df2pe] [< 3434 0 0 >]> [ ] { < tito > < pefp87df2pe > }
    */
    
    char delimiter;
    entrada >> m.distancia >> delimiter;
    entrada >> m.tamPosiciones >> delimiter;
    entrada >> m.indiceCabecera >> delimiter;
    // TODO: falta hacer operator<< y >> de trieMaraton
    // entrada >> m.animales >> delimiter >> m.ordenLlegada >> delimiter >> *(m.posiciones);
    return entrada;
};

/*
 *  De aqu� para abajo todas las funciones son de animar()
 */

Distancia Maraton::distCabecera(const conjlog<Nombre> ca){
     animalInfo info;
     string n;
     conjlog<string>::iterador_const iter(ca);
     int distAsuperar;
     
    animalInfo cabeza;
    string cabezaEjemplar = posiciones[indiceCabecera].dameUno();
    animales.Obtener(cabezaEjemplar, cabeza);
    distAsuperar = cabeza.cuantoCorrio;
     
         while (iter.valido()) {
          n = iter.actual();
          animales.Obtener(n,info);
          int distRecPorCab = info.cuantoCorrio + info.cuantoCorre;
          if (info.posicion == 1 && distRecPorCab > distAsuperar) 
          {distAsuperar = distRecPorCab;}
          iter.siguiente();
          }
          return distAsuperar;
/*esta funcion calcula, en el caso de que un cabecera haya sido animado,
cuanto hay que correr para superar al mas rapido entre los cabeceras aniamdos.
si no hay cabeceras animados devuelve una distancia del grupo cabecera.
Su complejidad es O((#c*amax)+log(n))*/
     }



void Maraton::animar(const conjlog<Nombre> &corredores){
     Distancia recorre; 
     string nombre;
     ConjNombres grupoC(posiciones[indiceCabecera]);   
     nat posPreAnimar; 
     nat posEnArray;
     Distancia nuevaDist;
     ConjNombres losQueLlegaron;
     conjlog<string>::iterador_const iter(corredores);
     bool terminaMaraton;
     bool alcanzaOpasaCabecera;
     animalInfo info;
     
    
    
    while (iter.valido()) {
          nombre = iter.actual();
          
          animales.Obtener(nombre, info);
          recorre = info.cuantoCorre;
          nuevaDist = recorre + info.cuantoCorrio;
          posPreAnimar = info.posicion;
          posEnArray = (posPreAnimar + indiceCabecera - 1+tamPosiciones) % tamPosiciones;
          terminaMaraton = nuevaDist >= distancia;
          
          alcanzaOpasaCabecera = nuevaDist  >= distCabecera(corredores);
                              
          posiciones[posEnArray].sacar(nombre);
          
          if (alcanzaOpasaCabecera  &&  !(grupoC.pertenece(nombre))  &&  ! terminaMaraton ) {
              nuevaDist = nuevaDist + recorre;
          }
          //cambio la guarda pq me di cuenta que si lo borre no tiene sentido preguntar
          //si pertenece al grupo, ya se que no. La nueva deberia tener el mismo valor

          if (terminaMaraton) {
              animales.Modificar(nombre, nuevaDist, 0);
              modificarArrayGano(posEnArray);
              losQueLlegaron.agregar(nombre);
          } else 
          	  modificarArray(nombre, nuevaDist, posEnArray);
          
          animales.Obtener(nombre, info);
         
          iter.siguiente();
      }  
      
      if ( ! losQueLlegaron.esVacio() ) 
	  		ordenLlegada.insertarAlFinal(losQueLlegaron);
 
 };

void Maraton::modificarArrayGano(nat posEnArray) {
	bool solo;
	string nombre;
	animalInfo info;
	nat pos;
	Distancia dist;
	
	solo = ((posiciones[posEnArray].cardinal()) == 0);
	
	if ( solo ) { 
		correrArrayParaAdelante(posEnArray);     
	    int i = 0;     
	    while (((i+posEnArray+tamPosiciones) % tamPosiciones) != indiceCabecera) {
	    	conjlog<string>::iterador_const iter(posiciones[(i+posEnArray+tamPosiciones) % tamPosiciones]);
	        while (iter.valido()) {
	              nombre = iter.actual();
	              animales.Obtener(nombre,info);
	              pos = info.posicion;
	              dist = info.cuantoCorrio;
	              animales.Modificar(nombre,dist,(pos-1+tamPosiciones) % tamPosiciones); 
	              iter.siguiente();
	        }
	        i++;
		}
	 }
}

void Maraton::correrArrayParaAdelante(nat posEnArray) {
     int k;
     nat pos;
     Distancia dist;
     string n;
     animalInfo info;
       
     k = posEnArray;
if (posiciones[k].esVacio()) {
     while (((k+1+tamPosiciones) % tamPosiciones)!= indiceCabecera && !posiciones[(k+1+tamPosiciones)%tamPosiciones].esVacio()) {
           posiciones[(k+tamPosiciones) % tamPosiciones] = posiciones[(k+1+tamPosiciones) % tamPosiciones];
           conjlog<string>::iterador_const iter(posiciones[(k+tamPosiciones) % tamPosiciones]);
           while (iter.valido()) {
                 n = iter.actual();
                 animales.Obtener(n,info);
                 pos = info.posicion;
                 dist = info.cuantoCorrio;
                 animales.Modificar(n,dist,(pos-1+tamPosiciones) % tamPosiciones); 
                 iter.siguiente();
                 }
           k++;
     }  
     posiciones[(k) % tamPosiciones] = conjlog<string>();
  }
}

void Maraton::modificarArray(string nombre, Distancia nuevaD, nat posEnA) {

	if ( ! posiciones[indiceCabecera].esVacio() ) {
	    animalInfo info;
	    string nomNuevaC = posiciones[indiceCabecera].dameUno();
	    animales.Obtener(nomNuevaC,info);
	    Distancia distRecporCabeceraActualizada = info.cuantoCorrio; 
	         
	    if (nuevaD >= distRecporCabeceraActualizada  ) {
	    	modificarArrayNuevaCabecera(nombre, nuevaD, posEnA); }
	    else
	    	ponerDondeVaya(nombre, nuevaD, posEnA); 
	    
	} else {  		//si la cabecera es vacia estoy animando al unico cabecera
		modificarArrayNuevaCabecera(nombre, nuevaD, posEnA);
	}
}

void Maraton::modificarArrayNuevaCabecera(string n,Distancia nuevaD,nat posEnA) {
     bool solo = posiciones[posEnA].esVacio();
     conjlog<string> vacio; 
     string nom;
     animalInfo info;
     nat pos;
     Distancia dist;

     if (posEnA != indiceCabecera && solo) {} 
     else { 
       	  if (solo) 
       	      correrArrayParaAdelante(posEnA);
          indiceCabecera = (indiceCabecera-1+tamPosiciones) % tamPosiciones;
     }
      posiciones[indiceCabecera] = vacio.agregar(n);
      animales.Modificar(n,nuevaD,1);

      int i = 1;     
      while (((i+indiceCabecera+tamPosiciones) % tamPosiciones) != indiceCabecera) {
      conjlog<string>::iterador_const iter(posiciones[(i+indiceCabecera+tamPosiciones) % tamPosiciones]);
      while (iter.valido()) {
          nom = iter.actual();
          animales.Obtener(nom,info);
          pos = info.posicion;
          dist = info.cuantoCorrio;
          animales.Modificar(nom,dist,(pos+1+tamPosiciones) % tamPosiciones); 
          iter.siguiente();
      }
      i++;
      }
}

void Maraton::ponerDondeVaya(string n, Distancia nuevaD, nat posEnA) {
     bool noEstaSituado = true;
     int i = 1;
     int j = posEnA;
     animalInfo info;
     animales.Obtener(n,info);
     int posPreAnimar = info.posicion;
     int nuevaPos;
     conjlog<string> vacio;
     
     while ((((j+tamPosiciones) % tamPosiciones) != indiceCabecera)&& noEstaSituado) {
           if (distDelSiguiente(i,posEnA) > nuevaD) { 
              nuevaPos = (posEnA-i+1+tamPosiciones) % tamPosiciones;
              correrArrayParaAtras(posEnA,i);
              posiciones[nuevaPos] = vacio.agregar(n);
              animales.Modificar(n,nuevaD,(posPreAnimar-i+1+tamPosiciones) % tamPosiciones); 
              noEstaSituado = false;
              
              }
            else if(distDelSiguiente(i,posEnA)== nuevaD) {
                    posiciones[posEnA-i].agregar(n);
                    animales.Modificar(n,nuevaD,(posPreAnimar-i+tamPosiciones) % tamPosiciones);
                    noEstaSituado = false;
                    correrArrayParaAdelante(posEnA); 
                    }
             i++;
             j--;
            }
}


void Maraton::correrArrayParaAtras(nat posEnA, int i) {
     nat j = posEnA;
     nat pos;
     Distancia dist;
     animalInfo info;
     conjlog<string> varAux1;
     conjlog<string> varAux2 = posiciones[(j-i+1+tamPosiciones) % tamPosiciones];    
     varAux1.agregar("lo que sea por no estar vacia");
     string n;
     
     if ((i != 1)||(!posiciones[posEnA].esVacio())) { //esto es para que no lo haga al pedo
           while ((!varAux1.esVacio()) && (((j-i+2+tamPosiciones)%tamPosiciones) != indiceCabecera)) {
                 varAux1 = posiciones[(j-i+2+tamPosiciones) % tamPosiciones];
                 posiciones[(j-i+2+tamPosiciones) % tamPosiciones] = varAux2;
                 varAux2 = varAux1;
                 j++;
                 conjlog<string>::iterador_const iter(posiciones[(j-i+1+tamPosiciones) % tamPosiciones]);
                 while (iter.valido()) {
                       n = iter.actual();
                       animales.Obtener(n,info);
                       pos = info.posicion;
                       dist = info.cuantoCorrio;
                       animales.Modificar(n,dist,(pos+1+tamPosiciones) % tamPosiciones); 
                       iter.siguiente();
                 }
        
     }
   }
}

Distancia Maraton::distDelSiguiente(int i, nat posEnA) {
    animalInfo info;
    if (posEnA == indiceCabecera) return 0;
    else animales.Obtener(posiciones[(posEnA-i+tamPosiciones)% tamPosiciones].dameUno(), info);
         
         return info.cuantoCorrio;
         
}  
    


