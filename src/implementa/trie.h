#ifndef _TRIE_H
#define _TRIE_H

#include "lib/conjlog.h"
#include "lib/secu.h"

using namespace std;

/*
 * TODO: "TODOS sus tipos deben ofrecer
 * los operadores << y >> para entrada salida, as? como tambi?n los
 * operadores de asignaci?n, construcci?n por copia, y operadores == y !="
 */
 
template <class T>
class trie{
public: 
        void Definir(string clave, T significado);
        void Modificar(string clave, T significado);
        bool Obtener(string clave, T &elem) const;
        bool Definido(string clave);
        void Claves(conjlog<string> &destino) const;
        template <class T1>
        friend ostream& operator<<(ostream& salida, const trie<T1> &elem);
        template <class T1>
        friend istream& operator>>(istream& entrada, trie<T1> &elem);
        bool operator==(const trie<T> &elOtro) const;
        trie<T>& operator=(const trie<T> &elOtro);
        trie(const trie<T>&);
        trie(){};
        
private: 
        void modificarOAgregar(string clave, T significado);
        class datoTrie{
             public: 
                     char clave;
                     trie<T> *proximo;
                     T *significado;
                     datoTrie();//Para que la secu lo tome le tengo que poner un constructor sin parametros
                     datoTrie(char letra);
                     datoTrie(const datoTrie&);
                     ~datoTrie();
                     ostream& operator<<(ostream& salida);
                     typename trie<T>::datoTrie&
                     operator=(const datoTrie&);
                     bool operator <(const datoTrie& elem2);
                     bool operator ==(const datoTrie& elem2);
        };
        secu<datoTrie> datos;
        void asignarPosicion(datoTrie *&res, char c);
        bool buscar(datoTrie *&res, string clave) const;
        bool encontrarCaracterEnSecu(datoTrie *&res, char c, const secu<datoTrie> &data) const;
        void clavesEnSecu(string prefijo, const secu<datoTrie> &datos, conjlog<string> &destino) const; 

};

//Implementaciones

template <class T>
trie<T>::trie(const trie<T> &trieACopiar){
   datos = trieACopiar.datos;

}

template <class T>
trie<T>& trie<T>::operator=(const trie<T> &elOtro){
  
   if(this!=&elOtro){
       datos.vaciar();
       datos = elOtro.datos;
    }
    return *this;
}

template <class T>
bool trie<T>::operator==(const trie<T> &elOtro) const{
    return datos == elOtro.datos;
}

template <class T>
ostream& operator<<(ostream& salida, const trie<T> &elem){
    /*
     * El formato definido para el trie es:
     *
     *  < [ clave0 ] [ significado0 ] >< [ clave1 ] [ significado1 ] >...< [ claveN ] [ significadoN ] >
     *
     * por ejemplo :
     *
     * <[pepe] [4 32]><[laBestia] [5 666]>
     */
    conjlog<string> temp;
    elem.Claves(temp);
    conjlog<string>::iterador_const it(temp);
    while( it.valido() ) {
    	T significado;
    	elem.Obtener(it.actual(),significado);
    	salida << "< [ " << it.actual() << " ] [ " << significado << " ] >";
    	it.siguiente();
	}
	return salida;
}

template <class T>
istream& operator>>(istream& entrada, trie<T> &t) {
    /*
     * El formato esperado es:
     *
     *  < [ clave0 ] [ significado0 ] > < [ clave1 ] [ significado1 ] >...< [ claveN ] [ significadoN ] >
     *
     * por ejemplo :
     *
     * < [ pepe ] [ 4 32 ] >< [ laBestia ] [ 5 666 ] >
     *
     */
	char delimiter;
	string clave;
	T significado;

    entrada.get(); // Espero un ' '

    while ( entrada.get() == '<' ) {
        entrada >> delimiter;  // espero un '['

        entrada >> clave;

        entrada >> delimiter;  // espero un ']'
        entrada >> delimiter;  // espero un '['

        entrada >> significado;
        t.Definir( clave, significado );

        entrada >> delimiter;  // espero un ']'
        entrada >> delimiter;  // espero un '>'
    };

	return entrada;
};	
         
         
template <class T>
bool trie<T>::Obtener(string clave, T &elem) const{
    datoTrie *puntero;
    if (buscar(puntero, clave)){
        elem = *puntero->significado;
        return true;
    }else return false; 
}

template <class T>
bool trie<T>::Definido(string clave){
    datoTrie *puntero;
    return buscar(puntero, clave);
}

template <class T>
void trie<T>::Definir(string clave, T significado){
     modificarOAgregar(clave, significado);
}

template <class T>
void trie<T>::Modificar(string clave, T significado){
     modificarOAgregar(clave, significado);
}

template <class T>
void trie<T>::modificarOAgregar(string clave, T significado){
    datoTrie *aInsertar;
    asignarPosicion(aInsertar,clave[0]);//Asigna al puntero 'aInsertar' el datoTrie de la secuencia que tiene caracter igual a 'clave[0]'
    int tam = clave.size();//Longitud del string
    
    //Si el tama?o es 1, es que estoy trabajando con el ?ltimo caracter de la clave y tengo
    //que ingresar la informaci?n del significado asociado a la clave.
    if (tam==1){
        if(aInsertar->significado==NULL){//Si su puntero a significado es null es que estoy
                                         //definiendo la clave, sino que la estoy modificando
            T *sig = new T;
            *sig = significado;
            aInsertar->significado = sig;
        }else{
            *aInsertar->significado = significado;
        }
    }else{
        clave.erase(0,1);//Le saco el primer caracter al string
        if(aInsertar->proximo==NULL){
            trie<T> *nuevo = new trie<T>;
            aInsertar->proximo = nuevo;
        }
        aInsertar->proximo->modificarOAgregar(clave, significado);
    }
}

template <class T>
bool trie<T>::buscar(datoTrie *&res, string clave) const {
   unsigned int indiceChar = 0;
   secu<datoTrie> const *actual = &datos;
   unsigned int charsEncontrados = 0;
   while(indiceChar < clave.size() && actual != NULL ) {
        if(encontrarCaracterEnSecu(res,clave[indiceChar],*actual)){
            charsEncontrados++;
            if(res->proximo!=NULL)
				actual = &res->proximo->datos;
            else 
				actual = NULL;
        }else{
            actual = NULL;
            res = NULL;
        }
        indiceChar++;
   }
   if(charsEncontrados == clave.size() && res->significado != NULL)
      return true;
   else{
      res=NULL;
      return false;
   }
   /*Si en el trie tengo definido pepe y busco pepernio, solo va a haber encontrado
     4 caracteres en el trie("pepe"), entonces si la cantidad de caracteres encontrados 
     es dist. a la longitud de la clave, 'res' tiene que apuntar a null. Si esta definido
     solo pepernio y busco pepe, la cantidad de caracteres va a ser 4 ("pepe"), pero el
     significado para la cadena pepe no est? definido entonces 'res' tiene que apuntar a null*/
}

template <class T>
void trie<T>::asignarPosicion(datoTrie *&res, char c){
    if(!encontrarCaracterEnSecu(res,c,datos)){
        datos.insertar(datoTrie(c));
        res = &datos.primero();
    }
}

template <class T>
bool trie<T>::encontrarCaracterEnSecu(datoTrie *&res, char c, const secu<datoTrie> &data) const{
    bool encontrado = false;
    typename secu<datoTrie>::iterador_const it(data);
    	while (it.valido()&&!encontrado) {
    		if(it.actual().clave==c)encontrado = true;
    		else it.siguiente();
      	}
    if(encontrado){
         res = &it.actual();
         return true;
    }
	else{
        res = NULL;
        return false;
    } 
}

template <class T>
void trie<T>::Claves(conjlog<string> &destino) const{
    clavesEnSecu("", datos, destino);
}

template <class T>
void trie<T>::clavesEnSecu(string prefijo, const secu<datoTrie> &datos, conjlog<string> &destino) const{
    typename secu<datoTrie>::iterador_const it(datos);
    while (it.valido()){
       string temp = prefijo;
       temp.append(1,it.actual().clave);//Le agrego el caracter del elemento actual de la secu
       if(it.actual().significado!=NULL)destino.agregar(temp);
       else clavesEnSecu(temp, it.actual().proximo->datos, destino);
       it.siguiente();
    }
}

/*======================
Funciones clase datoTrie
======================*/

template <class T>
trie<T>::datoTrie::datoTrie(){
     proximo = NULL;
     significado = NULL;
}

template <class T>
trie<T>::datoTrie::datoTrie(char letra){
     clave = letra;
     proximo = NULL;
     significado = NULL;
}

template <class T>
trie<T>::datoTrie::datoTrie(const datoTrie &elOtro){
     clave = elOtro.clave;
     if(elOtro.proximo!=NULL){
         proximo = new trie<T>;
        *proximo = *elOtro.proximo;
     }else{
        proximo = NULL;
     }
     if(elOtro.significado!=NULL){
        significado = new T;
        *significado = *elOtro.significado;
     }else{
        significado = NULL;
     }
}

template <class T>
typename trie<T>::datoTrie&
trie<T>::datoTrie::operator=(const datoTrie &elOtro){
if(this!=&elOtro){
     delete proximo;
     delete significado;
     clave = elOtro.clave;
     if(elOtro.proximo!=NULL){
         proximo = new trie<T>;
        *proximo = *elOtro.proximo;
     }else{
        proximo = NULL;
     }
     if(elOtro.significado!=NULL){
        significado = new T;
        *significado = *elOtro.significado;
     }else{
        significado = NULL;
     }
}
return *this;
}

template <class T>
trie<T>::datoTrie::~datoTrie(){
     delete proximo;
     delete significado;
}

template <class T>
bool trie<T>::datoTrie::operator <(const datoTrie& elem2){
     return clave<elem2.clave;
}

template <class T>
bool trie<T>::datoTrie::operator ==(const datoTrie& elem2){
     bool iguales = (clave==elem2.clave);
     if(proximo==NULL){
        if (elem2.proximo!=NULL)return false;
     }else{
        if(elem2.proximo==NULL)return false;
        else iguales = iguales && (*proximo==*elem2.proximo);
    }
    if(significado==NULL){
        if (elem2.significado!=NULL)return false;
    }else{
        if(elem2.significado==NULL) return false;
        else iguales = iguales && (*significado==*elem2.significado);
    }

    return iguales;
}

template <class T>
ostream& trie<T>::datoTrie::operator<<(ostream& salida){
     salida<<clave;
     return salida;
}


#endif
