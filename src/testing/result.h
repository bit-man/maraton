// result.h :
// v1.0.1 -10nov2004- versi�n publicable
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -23may2004- creado

#ifndef RESULT_H
#define RESULT_H

#include <string>

using std::string;

class Result
{
public:
	enum Type { invalid = -1, ok = 0, error = 1, unknown = 2 };
	
	// TODO: la inicializacion de los miembros en los constructores se podr�a
	// cambiar a una forma m�s eficiente.
	
	Result(Type resultType, string message = "") 
	{ 
		type = resultType; msg = message; 
	}
	
	Result(const Result& otherResult) 
	{ 
		type = otherResult.type; msg = otherResult.msg; 
	}
	
	Type getType() const { return type; }
    string getMessage() const { return msg; }

private:
	Type type;
	string msg;
};

#endif
