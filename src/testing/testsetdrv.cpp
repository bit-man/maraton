// v1.0.0

#include "testsetdrv.h"
#include <iostream>
#include <fstream>
#include "util.h"
#include "testcasedrv.h"
#include "usageException.h"
#include "fileFormatException.h"

using namespace std;

Histogram TestSetDrv::exec(const std::string& fileName) 
	throw(UsageException, FileFormatException, Exception)
{
	const string fileHeadSignature = "[testset]";	/// debe estar en minuscula

    ifstream ifs(fileName.c_str());

	if (!ifs) throw UsageException("no se encuentra el archivo: " + fileName);

	string textLine;

	getline(ifs, textLine);
	tolowerstr(trim(textLine));

    if (textLine != fileHeadSignature) 
		throw FileFormatException(fileName, 
				"encabezado incorrecto (no es [testset])");

	Histogram resultCount;

	while (!ifs.eof()) {
		getline(ifs, textLine);
		trim(textLine);

		if (!textLine.empty() and textLine[0] != '#') {

			#ifndef NDEBUG
			cout << " DEBUG " << " casoTest: " << textLine << endl; //! debug
			#endif

			Result r = TestCaseDrv::exec(textLine);

			switch (r.getType()) {
				case Result::error:
				case Result::invalid:
					cerr << r.getMessage() << endl;
					break;
				case Result::unknown:
					cout << r.getMessage() << endl;
					break;
				case Result::ok:
					break;
			}

			resultCount[r.getType()]++;
		}
	}
	return resultCount;
}
