// v1.0.2
#include "testcasedrv.h"
#include <iostream>			//! s�lo para hacer debugging
#include <fstream>
#include <sstream>
#include "util.h"
#include "cmd.h"
#include "result.h"
#include "objectTable.h"
#include "exception.h"
#include "fileFormatException.h"
#include "errorTestException.h"
#include "invalidTestException.h"

Result TestCaseDrv::exec(const string& fileName) throw (Exception)
{
	const string fileHeadSignature = "[testcase]";	/// debe estar en minuscula

	int lineNumber = 0;
	string textLine;
	Result result(Result::ok);

	try {
		ifstream ifs(fileName.c_str());
        if (!ifs) throw InvalidTestException("no se encuentra el archivo");

		ObjectTable symbolTable;

		getline(ifs, textLine);
		tolowerstr(trim(textLine));
		if (textLine != fileHeadSignature)						
			throw FileFormatException(fileName, 
					"encabezado incorrecto (no es [testcase])");	

		lineNumber++;

		while (!ifs.eof()) {
			getline(ifs, textLine);
			trim(textLine);
			lineNumber++;

			if (!textLine.empty() and textLine[0] != '#') {
				#ifndef NDEBUG
				cout << " DEBUG " << " cmd: " << textLine << endl; //! DEBUG
				#endif
				cmd::exec(textLine, symbolTable, result);
			}
		}
	}

	// TODO: hay que factorizar el c�digo que sigue. est� despolijro.

	catch (ErrorTestException& e) {
		return Result(Result::error, "caso de test: " + fileName + '\n' + 
			"linea " + toString<int>(lineNumber) + ": " + textLine + '\n' + 
			"problema: " + e.message());
	}
	catch (InvalidTestException& e) {
		// TODO tengo la sospecha de que este c�digo es inalcanzable. revisarlo.
		// debiera venirse ac� cuando hay par�metros de m�s, de menos, errores
		// de tipo y cosas similares. en todo caso, puede que haya que corregir
		// cmd.cpp en lugar de quitar esta secci�n.
		return Result(Result::invalid, "caso de test: " + fileName + '\n' + 
				"linea " + toString<int>(lineNumber) + ": " + textLine + '\n' + 
				"problema: " + e.message());
		// FIXME: ojo. invalidTest tambi�n es arrojada por fileNotFound y se ve
		// fe�to que el error diga "linea 0: problema: bla, bla". no me gusta el
		// "linea 0". :-(
	}
	catch (std::ios_base::failure& e) {
		return Result(Result::invalid, "caso de test: " + fileName + '\n' +
			"linea " + toString<int>(lineNumber) + ": " + textLine + '\n' +
			"problema: E/S incorrecta - " + e.what() + '\n' +
			"esta es una excepcion tal vez causada por el operador >> o el <<, por la falta de un parametro, " +
			"por haberse encontrado un tipo de datos no esperado, o vaya uno a saber por que. " +
			"quiero creer que escribiste 'true' y 'false' BIEN y en minusculas.");
	}
	catch (BadTypeException& e) {
		return Result(Result::invalid, "caso de test: " + fileName + '\n' +
			"linea " + toString<int>(lineNumber) + ": " + textLine + '\n' + 
			"problema: " + e.message());
	}
    catch (FileFormatException& e) {
		return Result(Result::invalid, "caso de test: " + e.fileName() + '\n' + 
			"problema: formato invalido - "  + e.message());
    }
	catch (UsageException& e) {
		throw UsageException("caso de test: " + fileName + '\n' + 
				"linea " + toString<int>(lineNumber) + ": " + textLine + '\n' +
				"problema:  " + e.message()); // sube como Exception
		// TODO para uniformar, ver si conviene que esta excepci�n tambi�n
		// suba como Result::invalid o algo as�. si se encuentra una buena raz�n
		// para que no suceda, habr�a que documentarla.
	}

	return result;
}
