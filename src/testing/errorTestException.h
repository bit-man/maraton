// errorTestException.h : 
// v1.0.0 -24jun2004- versi�n publicable
// v0.2.0 -22jun2004- creado a partir de invalidTestException

#ifndef ERROR_TEST_EXCEPTION_H
#define ERROR_TEST_EXCEPTION_H

#include "exception.h"

class ErrorTestException : public Exception
{
public:
	ErrorTestException(string message = ""): Exception(message) { }
};

#endif
